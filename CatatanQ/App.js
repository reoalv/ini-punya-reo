import 'react-native-gesture-handler';
import React from 'react';
import {Provider, useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import Home from './src/Screen/Home';
import Location from './src/Screen/Location';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/Redux/Store';
import SplashScreen from 'react-native-splash-screen';
import LoginScreen from './src/Screen/LoginScreen';

const Stack = createStackNavigator();

const transitionScreen = {
  ...TransitionPresets.SlideFromRightIOS,
};

export default function App() {
  setTimeout(() => {
    SplashScreen.hide();
  }, 2000);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Root />
      </PersistGate>
    </Provider>
  );
}

function Root() {
  const Token = useSelector(
    state => state.ReducerLogin.data.payload.access_token,
  );
  const Route = Token ? 'Home' : 'Login';
  console.log('TOKEN', Token);
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={transitionScreen}>
        <Stack.Screen
          options={{headerShown: false}}
          name="Home"
          component={Home}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Location"
          component={Location}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
