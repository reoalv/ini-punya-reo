import axios from 'axios';
import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {useDispatch} from 'react-redux';
import Poppin from '../Component/Poppin';
import {postLogin} from '../Redux/actionLogin';

export default function LoginScreen({navigation}) {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const pressLogin = () => {
    setLoading(true);
    let myToken = null;
    axios
      .post('https://tasklogin.herokuapp.com/api/login', {
        username: 'johndoe',
        password: 'password',
      })
      .then(function (res) {
        console.log('RES', res.data);
        myToken = res.data?.access_token;
        dispatch(postLogin(res.data));

        if (myToken) {
          navigation.navigate('Home');
        }
        setLoading(false);
      })
      .catch(function (e) {
        console.log(e);
        setLoading(false);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <Poppin type="SemiBold">Loading. . .</Poppin>
      ) : (
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.button}
          onPress={pressLogin}>
          <Poppin type="SemiBold" style={styles.textButton}>
            Login
          </Poppin>
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#133E7E',
    borderWidth: 5,
    borderColor: 'rgba(22, 70, 148, 1)',
    height: heightPercentageToDP(25),
    width: heightPercentageToDP(25),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: heightPercentageToDP(30),
    elevation: 20,
    shadowRadius: 10,
  },
  textButton: {
    fontSize: 24,
    color: 'white',
  },
});
