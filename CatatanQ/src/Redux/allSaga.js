import {all} from 'redux-saga/effects';
import sagaHome from './sagaHome';
import sagaLogin from './sagaLogin';

export function* allSaga() {
  yield all([sagaHome(), sagaLogin()]);
}
