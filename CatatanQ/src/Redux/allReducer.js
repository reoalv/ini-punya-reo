import {combineReducers} from 'redux';
import ReducerHome from './reducerHome';
import ReducerLogin from './reducerLogin';

export const allReducer = combineReducers({
  ReducerHome,
  ReducerLogin,
});
