export const POST_LOGIN = 'POST_LOGIN';
export const SET_LOGIN = 'SET_LOGIN';

export const postLogin = payload => {
  return {
    type: 'POST_LOGIN',
    payload,
  };
};

export const setLogin = payload => {
  return {
    type: 'SET_LOGIN',
    payload,
  };
};
