import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {navigate} from '../Component/Navigate';
import {POST_LOGIN, setLogin} from './actionLogin';

function* postData(action) {
  console.log('ACTION', action);
  try {
    yield put(setLogin(action));
  } catch (e) {
    console.log(e);
  }
}

function* sagaLogin() {
  yield takeLatest(POST_LOGIN, postData);
}
export default sagaLogin;
