import {SET_LOGIN} from './actionLogin';

const initialState = {
  data: [],
};

const reducerLogin = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default reducerLogin;
